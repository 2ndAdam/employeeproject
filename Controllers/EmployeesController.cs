﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

using EmployeeProject.Models;

namespace EmployeeProject.Controllers
{
    public class EmployeesController : Controller
    {
        // GET: Employees
        public ActionResult AdminIndex()
        {
            using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
            {
                return View(dbModel.Employees.ToList());
            }  
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: Employees/Details/5
        public ActionResult Details(int id)
        {
            using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
            {
                return View(dbModel.Employees.Where(x => x.EmployeeID == id).FirstOrDefault());
            }
                
        }

        // GET: Employees/Create
        public ActionResult Register()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        public ActionResult Register(Employee employee)
        {
            try
            {
                // TODO: Add insert logic here
                using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
                {
                    dbModel.Employees.Add(employee);
                    dbModel.SaveChanges();
                }

                    return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int id)
        {
            using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
            {
                return View(dbModel.Employees.Where(x => x.EmployeeID == id).FirstOrDefault());
            }
        }

        // POST: Employees/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Employee employee)
        {
            using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
            {
                try
                {
                    dbModel.Set<Employee>().Attach(employee);
                    dbModel.Entry(employee).State = EntityState.Modified;
                    dbModel.SaveChanges();

                    return RedirectToAction("Index");
                }

                catch
                {
                    return View();
                }

            }
        }
            //try
            //{
                // TODO: Add update logic here
              //  using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
                //{
                    //dbModel.Entry(employee).State = EntityState.Modified;
                    //dbModel.SaveChanges();

                  //  dbModel.attach(employee);
                    // dbModel.Entry(employee).State = EntityState.Modified;
                    //if (dbModel != null)
                    //{
                      //  Context.Entry(dbModel).CurrentValues.SetValues(employee)
                   // }
                    //ContextBoundObject.SaveChanges(EmployeeID);
                    
                //}

                  //  return RedirectToAction("Index");
           // }
            //catch
            //{
              //  return View();
            //}
       // }

        // GET: Employees/Delete/5
        public ActionResult Delete(int id)
        {
            using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
            {
                return View(dbModel.Employees.Where(x => x.EmployeeID == id).FirstOrDefault());
            }
        }

        // POST: Employees/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (EmployeeProjectDBEntities dbModel = new EmployeeProjectDBEntities())
                {
                    Employee employee = dbModel.Employees.Where(x => x.EmployeeID == id).FirstOrDefault();
                    dbModel.Employees.Remove(employee);
                    dbModel.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
